type_used(T, T).
type_used(T, fun(Tin, _)) :- type_used(T, Tin).
type_used(T, fun(_, Tout)) :- type_used(T, Tout).
type_used(T, tabs(_, U)) :- type_used(T, U).

type_not_bound(tuvar(_), unit).
type_not_bound(tuvar(N), fun(Tin, Tout)) :- type_not_bound(tuvar(N), Tin), type_not_bound(tuvar(N), Tout).
type_not_bound(tuvar(_), tuvar(_)).
type_not_bound(tuvar(N), tabs(tuvar(M), T)) :- dif(N, M), type_not_bound(tuvar(N), T).

type_wf(Gamma, unit).
type_wf(Gamma, fun(Tin, Tout)) :- type_wf(Gamma, Tin), type_wf(Gamma, Tout).
type_wf(Gamma, tuvar(N)) :- list_member(tuvar(N), Gamma).
type_wf(Gamma, tabs(tuvar(N), T)) :- type_wf([tuvar(N) | Gamma], T), type_used(tuvar(N), T), type_not_bound(tuvar(N), T).
